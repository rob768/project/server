# Server

![ROS](https://img.shields.io/badge/ROS-dashing-orange.svg)

The **Server** repository is a part of *Aalborg University - Robotics 7* project *Fleet Management of Distributed Multi-Robot Systems for Autonomous Data Collection* that coexists with services contained within [**Turtle**](https://gitlab.com/rob768/project/turtle) repository. This repository consists of services that are executed on a server.

> Note: This repository was tested on `x86_64` (`amd64`) architecture with *Ubuntu 18.04*. However, the implementation is platform and architecture agnostic.


## Services

This repository contains three services that are grouped under a single [`docker-compose`](docker-compose.yml).

- [**navigation2_slam_server**](navigation2_slam_server) - Provides `navigation2` service with SLAM (`cartographer`) for external robots on specific `ROS_DOMAIN_ID`.
- [**rosbag2_recorder**](rosbag2_recorder) - Records all specified topics on all desired `ROS_DOMAIN_ID` with the use of [rosbag2](https://github.com/ros2/rosbag2).
- [**rosbag2_viewer**](rosbag2_viewer) - Forwards CLI application to *port 80* (using [*GoTTY*](https://github.com/yudai/gotty)) to provide overview and playback of all recordings. If utilised with *balenaCloud*, this service can be accessed through public URL.

> Note: The `navigation2_slam_server` and `rosbag2_recorder` services require robots to be accessible via a local area network.


## Dependencies

Dependencies for this repository are based on the deployment method. 
- If utilising [*balenaCloud*](https://www.balena.io/cloud/), follow [this](https://github.com/balena-io/balena-cli/blob/master/INSTALL.md) guide.
- If building locally, follow guides for [Docker Engine](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).
- Or use any platform that supports `Docker` containers.


## Building, Deployment And Running

If utilising [*balenaCloud*](https://www.balena.io/cloud/), follow these steps:
```sh
$ git clone https://gitlab.com/rob768/project/server.git && cd server
$ balena push <application>
```

If building locally, follow these steps:
```sh
$ git clone https://gitlab.com/rob768/project/server.git && cd server
# docker-compose up
```

## Versioning

This repository utilises Semantic Versioning, please see [CHANGELOG](CHANGELOG.md) for more information.


## License

[MIT License](LICENSE)
