## This could be substituted with some awesome application
# Source ROS2 for `ros2 bag` commands
. /ros_entrypoint.sh

# Enter the directory where rosbags are stored
cd ${PERSISTENT_STORAGE_DIR}

# Open bash for use
/bin/bash
